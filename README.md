![MNCZqEJ.png](https://bitbucket.org/repo/qAbGGK/images/3834611362-MNCZqEJ.png)

# README #

Annotated.js converts formatted code into an annotated table. This is useful for displaying code and explaining it line by line.


## Dependencies
* jQuery (any recent version)


## Usage

You need at least the default stylesheet, jQuery, and annotated.js to be referenced in your html
```
#!html

<link href="css/annotated.css" rel="stylesheet" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="annotated.js"></script>
```


### Basic

```
#!javascript

$('#targetContainer').annotated({
   code: '<your code here>'
});
```

### With Options

```
#!javascript

$('#targetContainer').annotated({
   code: '<your code here>',
   appendTo: $('#someOtherElementToAddThisTo'),
   tabChar: { // this is still under development
      tabCharacter: '\t',
      length: 4
   },
   qTipOptions: <your qTip options>
});
```

### Code Formatting
Annotated.js takes a string, complete with line breaks and tab characters. It uses comments to render annotations:

```
#!csharp

public class Example
{
  // this comment line will be preserved
  public void DoStuff(int input)
  {
      var inputString = input.ToString(); // this inline comment will be rendered as a tooltip
  }
}
```


## Enhancements
* Add [qTip](http://qtip2.com/demos) to your project to enhance your tooltips' style, position, behavior, etc. Annotated will accept qTip options and apply them for you
* Add [highlightjs](http://highlightjs.org/) to your project to add language keyword highlighting. Annotated will automatically apply it for you.