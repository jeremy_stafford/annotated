﻿(function() {
    var defaultOptions = {
        code: '',
        appendTo: $(),
        tabChar: new tabCharacter('\t'),
        qTipOptions: {
            style: {
                classes: 'qtip-bootstrap'
            },
            position: {
                viewport: true,
                my: 'top right',
                at: 'top left',
                adjust: {
                    x: -20
                }
            }
        }
    };

    // annotated plugin constructor
    function annotated($target, options) {
        var opt = parseOptions(options);
        var newList = document.createElement('ul');
        if (opt.code) {
            var lines = getLines(opt.code, opt.tabCharacter);
            lines.forEach(function(line) {
                newList.appendChild(line);
            });
        }
        var $annotated = $(newList);
        $annotated.addClass('annotated');
        $target.append($annotated);
        if (opt.appendTo) {
            $(opt.appendTo).append($annotated);
        }

        // attach qTip
        if(qtip) bindAnnotations($annotated, opt.qTipOptions);

        // apply highlighting
        if(hljs) hljs.highlightBlock($annotated.get(0));
    }

    function bindAnnotations($annotated, options) {
        $annotated.find('li[title]').qtip(options);
    }

    // tabCharacter constructor
    function tabCharacter(tabChar, length) {
        this.character = tabChar;
        this.length = length;
    }

    function parseOptions(options) {
        if (!options) return defaultOptions;
        return {
            code: options.code || defaultOptions.code,
            appendTo: options.appendTo || defaultOptions.appendTo,
            tabCharacter: options.tabCharacter || defaultOptions.tabChar,
            qTipOptions: options.qTipOptions || defaultOptions.qTipOptions
        }
    }

    function getLines(code, tabChar) {
        code = replaceTab(code, tabChar);
        var lines = code.split('\n');
        var result = [];
        lines.forEach(function(line) {
            var lineInfo = parseLine(line);
            if (!lineInfo.hasInfo) return;
            var item = document.createElement('li');
            var lineText = document.createTextNode(lineInfo.info);
            item.appendChild(lineText);
            // annotate if comment exists
            if (lineInfo.comment) {
                item.setAttribute('title', lineInfo.comment);
            }
            result.push(item);
        });
        return result;
    }

    function parseLine(line) {
        var parts = line.split('//');
        var result = { info: null, comment: null, hasInfo: function () { return this.info || this.comment; } };
        if (isCommentLine(parts)) {
            result.info = line;
            result.comment = null;
        } else {
            result.info = parts[0];
            result.comment = parts[1] ? $.trim(parts[1]) : null;
        }
        return result;
    }

    function isCommentLine(parts) {
        var code = parts[0];
        var comment = parts[1];

        if (!comment) return false; // no comment at all, leave it alone
        
        // make sure it isn't an indented comment line
        var trimmed = $.trim(code.replace(/\&nbsp;/g, ''));
        return trimmed.length == 0;
    }

    function replaceTab(input, tab) {
        var tabMarkup = '\u00A0\u00A0\u00A0\u00A0 ';
        var replacement = '';
        if (tab.character === '\t') {
            replacement = input.replace(/\t/gi, tabMarkup);
        } else if(tab.character === '\s') {
            // TODO: e.g. replace every x instances with tabMarkup
        }
        return replacement;
    }

    /* Register Plugin */
    $.fn.annotated = function(options) {
        var $target = $(this);
        return new annotated($target, options);
    }
})(jQuery);

